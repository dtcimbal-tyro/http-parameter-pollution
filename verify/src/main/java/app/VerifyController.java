package app;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;

@RestController
public class VerifyController {

    @Value("${paymenturl}")
    private String paymentUrl;

    @PostMapping("/")
    public String res(HttpServletRequest request) {
        Action action = Action.from(request.getParameter("action"));
        Amount amount = Amount.from(request.getParameter("amount"));

        switch (action) {
            case TRANSFER: {
                return transfer(amount);
            }
            case WITHDRAW: {
                return withdraw();
            }
            default:
                return badAction();
        }
    }

    private String transfer(Amount amount) {
        System.out.println("Verify Controller: Going to transfer " + amount);
        RestTemplate restTemplate = new RestTemplate();
        String fakePaymentUrl = this.paymentUrl;  //Internal fake payment micro-service
        ResponseEntity<String> response
                = restTemplate.getForEntity(
                fakePaymentUrl + "?action=transfer&amount=" + amount.longValue(),
                String.class);
        return response.getBody();
    }

    private String withdraw() {
        return "Verify Controller: Sorry, you can only make transfer";
    }

    private String badAction() {
        return "Verify Controller: You must specify action: transfer or withdraw";
    }
}
