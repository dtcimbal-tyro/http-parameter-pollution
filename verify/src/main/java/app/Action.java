package app;

import app.exception.BadRequestException;

import java.util.Arrays;

public enum Action {
    TRANSFER("transfer"),
    WITHDRAW("withdraw");

    public static Action from(String param) {
        return Arrays.stream(values())
                .filter((it) -> it.action.equals(param))
                .findFirst()
                .orElseThrow(BadRequestException::new);
    }

    private final String action;

    private Action(String action) {
        this.action = action;
    }

    public String getAction() {
        return action;
    }

}
