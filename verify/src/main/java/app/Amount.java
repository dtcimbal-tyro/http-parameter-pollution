package app;

import app.exception.BadRequestException;

import java.math.BigInteger;

public class Amount {

    public static Amount from(String amount) {
        try {
            return new Amount(new BigInteger(amount));
        } catch (NumberFormatException ex) {
            throw new BadRequestException();
        }
    }

    private final BigInteger amount;

    private Amount(BigInteger amount) {
        this.amount = amount;
    }

    public long longValue() {
        return this.amount.longValue();
    }

    @Override
    public String toString() {
        return "$" + amount.longValue();
    }
}
